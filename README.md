# README #

To run this project, download the repository to your computer. Navigate to the "modern_portfolio" folder and run the "index.html" file.


### What is this repository for? ###

This repository is a personal portfolio project for the Front-End Development course offered by LUT.


### Who do I talk to? ###

If you run into trouble or need to communicate with the author, email them at: simranjit.singh@student.lut.fi